<?php

function cleanFormRequest( $val ) {
    $val = trim( $val );
    $val = strip_tags( $val );
    $val = htmlspecialchars( $val );

    return $val;
}

?>