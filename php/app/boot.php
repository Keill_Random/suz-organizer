<?php
include ('../init/php_init.php');

/* SERIALIZERS */
include ('../serializer/user.php');
include ('../serializer/table.php');

if ($user->id === 0) {
	echo '{"message" : "success", "data" : "false" }';
} else {
	echo '{"message" : "success", "data" : { "user": '.userSerializer().' , "tables": '.tableSerializer().' }  }';
}
?>