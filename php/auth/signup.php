<?php
include ('../init/php_init.php');
include ('../functions/form.php');
include ('../email/auth/activation_email.php');


if (!isset($_SESSION['userid'])) {
	
	$username = cleanFormRequest( $_POST['username'] );
	$password = cleanFormRequest( $_POST['password'] );
	$password = hash('sha512', $password);

	$ip = $_SERVER['REMOTE_ADDR'];
	
	$error = false;
	$errorMsg = false;

	$usernameError = '';
	$passwordError = '';

	//basic email validation
	if ( !filter_var($username,FILTER_VALIDATE_EMAIL) ) {
		$error = true;
		$errorMsg = !$errorMsg ? "Please enter valid email address." : $errorMsg;
	} else {

		// check email exist or not
		$query = "SELECT * FROM account WHERE email='$username' OR email_short='$username'";
		$result = mysqli_query($mysqli, $query) or die (mysqli_error($mysqli));
		$count = mysqli_num_rows($result);

		if ($count != 0) {
			$error = true;
			$errorMsg = !$errorMsg ? "Provided Email is already in use." : $errorMsg;
		}
	}

	// if email excist in account_unverified, send a new activation email

	// password validation
	if (empty($password)) {
		$error = true;
		$errorMsg = !$errorMsg ? "Please enter password." : $errorMsg;
	} else if (strlen($password) < 6) {
		$error = true;
		$errorMsg = !$errorMsg ? "Password must have atleast 6 characters." : $errorMsg;
	}

	if ($error) {
		echo "{message:'error', response:".$errorMsg."}";
	} else {
		$hash = md5($username);
		$email_short = str_replace(".","",explode("@", $username)[0])."@".explode("@", $username)[1];
		$created_date = date("Y-m-d H:i:s");
		
		mysqli_query($mysqli, "INSERT INTO account (
				email, 
				email_short, 
				password,
				created_timestamp,
				created_ip
				#hash
			) VALUES (
				'$username', 
				'$email_short', 
				'$password',
				'$created_date',
				'$ip'
				#'$hash'
			)") or die (mysqli_error($mysqli));

		echo "{message:'success'}";
	}

} else {
	echo '{"message":"error","response":"already logged in"}';
}
?>