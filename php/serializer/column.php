<?php
// include ('arrToJson.php');

function columnSerializer( $table_id , $json ) {
	global $mysqli;

	$user_id = $_SESSION['userid'];
	$column = array();

	$query = "SELECT * FROM table_column WHERE tableid='$table_id'";
	$result = mysqli_query($mysqli, $query) or die (mysqli_error($mysqli));

	$count = 0;
	while ($r = mysqli_fetch_assoc($result)) {
		$tmp 			= array();
		$tmp['id'] 		= $r['id'];
		$tmp['title'] 	= $r['title'];
		$tmp['arrange'] = $r['arrange'];
		
		array_push($column, $tmp);
	}

	if ($json)
		return json_encode($column, JSON_PRETTY_PRINT);
	else
		return $column;
}

?>