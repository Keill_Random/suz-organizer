<?php
include ('column.php');

function tableSerializer() {
	global $mysqli;

	$userid = $_SESSION['userid'];
	$table = array();

	$query = "SELECT * FROM main_table WHERE userid='$userid'";
	$result = mysqli_query($mysqli, $query) or die (mysqli_error($mysqli));

	$count = 0;
	while ($r = mysqli_fetch_assoc($result)) {
		$tmp 			= array();
		$tmp['id'] 		= $r['tableid'];
		$tmp['title'] 	= $r['title'];
		$tmp['type']    = $r['type'];

		$column 		= columnSerializer($r['tableid'] , false);
		$tmp['column']  = $column;

		array_push($table, $tmp);
	}

	return json_encode($table, JSON_PRETTY_PRINT);
}

?>