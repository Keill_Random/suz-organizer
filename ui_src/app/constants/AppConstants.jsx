// import keyMirror from 'keymirror';
var keyMirror = require('keyMirror');

module.exports = {

    endPoints: {
        BOOT                    : 'php/boot/boot'
    },

    actions: keyMirror({
        BOOTED                  : null
    }),

    events: keyMirror({
        CHANGE_EVENT            : 'changeEvent'
    })
};
