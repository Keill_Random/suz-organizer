import AjaxPromises from '../utils/AjaxPromises'
import { endPoints as EndPoints } from '../constants/AppConstants'

export default {

    boot: () => {
        return AjaxPromises.GET ( EndPoints.BOOT )
    }
}
