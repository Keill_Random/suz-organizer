import React from 'react';

/* STORES */
// import AuthStore from '../../stores/AuthStore'
// import AppStore from '../../stores/AppStore'
// import OwnerStore from '../../stores/OwnerStore'

/* ACTIONS */
import AppActions from '../../actions/AppActions'
// import OwnerActions from '../../actions/OwnerActions'

/* COMPONENTS */
import Auth from '../auth/Auth.react'

class App extends React.Component {

    constructor(props) {
        super(props);
        // this.state = { amount: 2 };
    }

    componentDidMount() {
        AppActions.boot()
    }

    render() {
        return (
            <div>
                <Auth />
            </div>
        );
    }
}

export default App;









// class App extends React.Component {

//     constructor(props) {
//         super(props);
//         this.state = { amount: 20 };
//     }

//     render() {
//         return (
//             <div>
//                 <span className="fa fa-hand-spock-o fa-1g">
//                     Amount: {this.state.amount}
//                 </span>
//                 <button onClick={() => this.setState(addOne)}>Add one</button>
//             </div>
//         );
//     }
// }

// const addOne = ({ amount }) => ({ amount: amount + 5 });

// export default App;

