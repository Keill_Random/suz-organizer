// import React from 'react';

import AuthMenu from './AuthMenu.react';
import Signup from './Signup.react';
import Signin from './Signin.react';
import Recover from './Recover.react';

class Auth extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            page: 'signup',
            amount: 0
        };

        this.goToPage = this.goToPage.bind(this);
    }

    goToPage(page) {
        this.setState({ page });
    }

    render() {
        return (
            <div
                className='auth'
                style={{'backgroundImage' : 'url(ui_src/assets/images/auth/lavender-fields.jpg)'}}>

                <div className='auth__content'>

                    <span className="fa fa-hand-spock-o fa-1g">
                        Amount: {this.state.amount}
                    </span>
                    <button onClick={() => this.setState(addOne)}>Add one</button>

                    <AuthMenu
                        page={this.state.page}
                        goToPage={this.goToPage} />

                    <Signin />

                </div>

            </div>
        );
    }
}

const addOne = ({ amount }) => ({ amount: amount + 15 });

export default Auth;
