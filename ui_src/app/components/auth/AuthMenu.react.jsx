import React from 'react';

// import AuthMenu from './AuthMenu.react';

class AuthMenu extends React.Component {

    // constructor(props) {
    //     super(props);
    //     // this.state = { amount: 0 };
    // }

    goToPage(page) {
        console.log('page' , page)
        this.props.goToPage(page);
    }

    render() {

        var page = this.props.page;

        return (
            <div className='auth-menu'>

                <div
                    className={'auth-menu__item '+ (page === 'signin' ? 'auth-menu__item--active' : '')}
                    onClick={this.goToPage.bind(this, 'signin')}>
                    Sign In
                </div>

                <div
                    className={'auth-menu__item '+ (page === 'signup' ? 'auth-menu__item--active' : '')}
                    onClick={this.goToPage.bind(this, 'signup')}>
                    Sign Up
                </div>

                <div
                    className={'auth-menu__item '+ (page === 'recover' ? 'auth-menu__item--active' : '')}
                    onClick={this.goToPage.bind(this, 'recover')}>
                    Recover
                </div>

            </div>
        );
    }
}

export default AuthMenu;
