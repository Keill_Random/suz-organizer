class Signin extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };

        this.onUsernameChange = this.onUsernameChange.bind(this);
    }

    onUsernameChange(e) {
        console.log('e', e)
        this.setState({ username : e.target.value.toLowerCase() });
    }

    render() {

        var page = this.props.page;

        return (
            <div className='auth-form'>

                <div className='auth-form__label'>Sign In</div>

                <input
                    type='text'
                    className='auth-form__input'
                    value={this.state.username}
                    onChange={this.onUsernameChange} />

                <div className='auth-form__butotn'>
                    Sign In
                </div>
            </div>
        );
    }
}

export default Signin;
