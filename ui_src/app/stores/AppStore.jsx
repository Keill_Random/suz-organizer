import AppDispatcher from '../dispatcher/AppDispatcher';
import { EventEmitter } from 'events';

import { actions as AppActions } from '../constants/AppConstants'

import { events as AppEvents } from '../constants/AppConstants'

let _state = {
    boot            : false
};

class AppStoreClass extends EventEmitter {

    boot( data ) {
        _state.boot = true
        this.emitChange()
    }

    getUser() {
        return _state.user
    }

    emitChange( callback ) {
        this.emit(AppEvents.CHANGE_EVENT, callback);
    }

    addChangeListener( callback ) {
        this.on(AppEvents.CHANGE_EVENT, callback)
    }

    removeChangeListener( callback ) {
        this.removeListener(AppEvents.CHANGE_EVENT, callback)
    }
}

const AppStore = new AppStoreClass();

AppStore.dispatchToken = AppDispatcher.register(action => {

    switch(action.actionType) {

        case AppActions.BOOTED:
            AppStore.boot(action.data)
            break

        default:
    }

});

export default AppStore;
