import AppDispatcher from '../dispatcher/AppDispatcher';
import { actions as ActionTypes } from '../constants/AppConstants';
import AppApi from '../api/AppApi';

export default {

    boot: () => {
        var promise = AppApi.boot()

        promise.then(res => {
            AppDispatcher.dispatch({
                actionType: ActionTypes.BOOTED,
                data: res.user
            })
        })
    }
}
